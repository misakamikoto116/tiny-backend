<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class TransactionCollection extends ResourceCollection
{
    private function transformData($data)
    {
        return [
            'id'                => $data->id,
            'no_transaction'    => $data->no_transaction,
            'name'              => $data->name,
            'amount'            => $data->amount,
            'file'              => $data->file,
            'date'              => $data->date,
            'is_active'         => $data->is_active,
            'user_id'           => $data->user_id ?? '-',
            'type_id'           => $data->type_id ?? '-',
            'amount_formatted'  => $data->amount_formatted ?? '-',
            'type'              => $data->type,
            'user'              => $data->user,
        ];
    }

    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => $this->transformCollection($this->collection),
            'pagination' => $this->paginationData()
        ];
    }

    /**
     * Remove links and meta from response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Illuminate\Http\Response  $response
     * @return void
     */
    public function withResponse($request, $response)
    {
        $jsonResponse = json_decode($response->getContent(), 2);
        unset($jsonResponse['links'],$jsonResponse['meta']);
        $response->setContent(json_encode($jsonResponse));
    }

    private function transformCollection($collection)
    {
        return $collection->transform(function ($data) {
            return $this->transformData($data);
        });
    }

    private function paginationData()
    {
        return [
            'total' => $this->total(),
            'count' => $this->count(),
            'per_page' => $this->perPage(),
            'current_page' => $this->currentPage(),
            'total_pages' => $this->lastPage()
        ];
    }
}
