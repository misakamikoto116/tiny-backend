<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your module. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Finance => Transaction Type
Route::apiResource('transaction-type', 'TransactionTypeController');

// Finance => Transaction
Route::apiResource('transaction', 'TransactionController')->except('show');
Route::prefix('transaction')->group(function () {
    Route::patch('done/{id}', 'TransactionController@done')->name('transaction.done');
    Route::get('sum_amount', 'TransactionController@sum_amount')->name('transaction.sum.amount');
});